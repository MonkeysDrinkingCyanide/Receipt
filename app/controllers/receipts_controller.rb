class ReceiptsController < ApplicationController
  def index
    @receipts = Receipt.where(:user_id => current_user.id).order(:purchase_date).reverse
  end

  def show
    @receipt = Receipt.find(params[:id])
  end

  def new
    @receipt = Receipt.new
  end

  def edit
    @receipt = Receipt.find(params[:id])
  end

  def create
    @receipt = Receipt.new(receipt_params)
    @receipt.user = current_user

    @receipt.purchase_date = receipt_params[:purchase_date].to_time
    @receipt.tax_rate = receipt_params[:tax_rate].to_f

    num = 0

    receipt_params[:items_attributes].map do |k, v|
      items = receipt_params[:items_attributes]
      item = items[k.to_s]
      @receipt.items[num].receipt = @receipt
      @receipt.items[num].price = item[:price].to_f
      @receipt.items[num].quantity = item[:quantity].to_i
      ++num
    end

    respond_to do |format|
      if @receipt.save
        format.html { redirect_to(@receipt, :notice => 'Receipt was successfully created.') }
      else
        flash[:notice] = @receipt.errors.full_messages
        format.html { render :action => 'new' }
      end
    end
  end

  def update
    @receipt = Receipt.find(params[:id])
    @receipt.user = current_user

    respond_to do |format|
      if @receipt.update_attributes(receipt_params)
        format.html { redirect_to(@receipt, :notice => 'Receipt was successfully updated.') }
      else
        format.html { render :action => 'new' }
      end
    end
  end

  def destroy
    @receipt = Receipt.find(params[:id])
    @receipt.destroy

    respond_to do |format|
      format.html { redirect_to(receipts_url) }
    end
  end

  def receipt_params
    params.require(:receipt).permit(
      :display_name, :purchase_date, :tax_rate, :payment_type, :user_id,
      items_attributes: [:id, :category, :quantity, :description, :price, :_destroy]
    )
  end
end
