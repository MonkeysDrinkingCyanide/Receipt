class ReportsController < ApplicationController
  def index

  end

  def show
    @receipts = Receipt.find(params[:id])
  end

  def show_monthly
  end

  def show_expense
  end
end
