Rails.application.routes.draw do
  devise_for :users

  resources :receipts

  resources :reports, :only => [:index, :show] do
    get 'monthly', :controller => "reports#show_monthly"
    get 'expense', :controller => "reports#show_expense"
  end

  root 'home#index'
end
