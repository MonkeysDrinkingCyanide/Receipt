class CreateReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :receipts do |t|
      t.string   :display_name
      t.datetime :purchase_date
      t.decimal  :tax_rate
      t.integer  :payment_type
      t.integer  :user_id

      t.timestamps
    end
  end
end
