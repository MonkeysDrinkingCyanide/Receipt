class Receipt < ApplicationRecord
  has_many :items
  belongs_to :user
  accepts_nested_attributes_for :items, :reject_if => :all_blank, :allow_destroy => true

  def subtotal
    _subtotal = 0.0

    items.each do |item|
      _subtotal += item.amount
    end
  end

  def tax
    subtotal * (tax_rate / 100)
  end

  def total
    subtotal + tax
  end
end
