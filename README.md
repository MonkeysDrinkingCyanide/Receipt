# Getting Started

- Install [Vagrant](https://www.vagrantup.com/)
- Install [Virtualbox](https://www.virtualbox.org/)
- Clone the repo and cd into the directory
- Run `vagrant up` to bring the virtual machine up
- Run `vagrant ssh` to ssh into the virtual machine
- Run `cd /vagrant` to change into the project directory
- Run `rake db:create` to create the database
- Run `rails s -b 0.0.0.0` to run the rails server
- Visit http://localhost:3000 in your web browser
