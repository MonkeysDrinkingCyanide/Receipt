class Item < ApplicationRecord
  belongs_to :receipt

  def amount
    price * quantity
  end
end
